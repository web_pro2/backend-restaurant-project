import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { Table } from './tables/entities/table.entity';
import { TablesModule } from './tables/tables.module';
import { Category } from './category/entities/category.entity';
import { CategoryModule } from './category/category.module';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './order-items/entities/order-item.entity';
import { OrderDetail } from './order-details/entities/order-detail.entity';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { User } from './users/entities/user.entity';
import { OrdersModule } from './orders/orders.module';
import { OrderItemsModule } from './order-items/order-items.module';
import { OrderDetailsModule } from './order-details/order-details.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ProductsModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'db.sqlite',
      entities: [
        Product,
        Table,
        Category,
        Order,
        OrderItem,
        OrderDetail,
        Category,
        Employee,
        User,
      ],
      synchronize: true,
    }),
    ProductsModule,
    TablesModule,
    CategoryModule,
    EmployeesModule,
    OrdersModule,
    OrderItemsModule,
    OrderDetailsModule,
    UsersModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
