export class CreateOrderDetailDto {
  productId: number;

  ord_detail_amount: number;

  ord_detail_table: number;
}
