import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOrderDetailDto } from './dto/create-order-detail.dto';
import { UpdateOrderDetailDto } from './dto/update-order-detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderDetail } from './entities/order-detail.entity';
import { Repository } from 'typeorm';
import { Product } from 'src/products/entities/product.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';

@Injectable()
export class OrderDetailsService {
  constructor(
    @InjectRepository(OrderDetail)
    private orderDetailRepository: Repository<OrderDetail>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}
  async create(createOrderDetailDto: CreateOrderDetailDto) {
    const product = await this.productRepository.findOneBy({
      product_id: createOrderDetailDto.productId,
    });
    const orderDetail: OrderDetail = new OrderDetail();
    orderDetail.ord_detail_amount = createOrderDetailDto.ord_detail_amount;
    orderDetail.ord_detail_name = product.name;
    orderDetail.ord_detail_total =
      product.price * orderDetail.ord_detail_amount;
    orderDetail.products = product;
    return this.orderDetailRepository.save(orderDetail);
  }

  findAll() {
    return this.orderDetailRepository.find({ relations: ['products'] });
  }

  findOne(id: number) {
    return this.orderDetailRepository.findOne({
      where: { order_detail_id: id },
      relations: ['products'],
    });
  }

  async getOrderDetailbyTableId(id: number) {
    const orderDetail = await this.orderDetailRepository.find({
      where: { ord_detail_table: id },
    });
    if (!orderDetail) {
      throw new NotFoundException(`No OrderItem in Table ${id}`);
    }
    return orderDetail;
  }

  async update(id: number, updateOrderDetailDto: UpdateOrderDetailDto) {
    const orderDetail = await this.orderDetailRepository.findOneBy({
      order_detail_id: id,
    });
    const updateOrderDetail = {
      ...orderDetail,
      ...updateOrderDetailDto,
    };
    return this.orderDetailRepository.save(updateOrderDetail);
  }

  async remove(id: number) {
    const orderDetail = await this.orderDetailRepository.findOneBy({
      order_detail_id: id,
    });
    return this.orderDetailRepository.softRemove(orderDetail);
  }
}
