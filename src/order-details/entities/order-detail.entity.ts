import { OrderItem } from 'src/order-items/entities/order-item.entity';
import { Product } from 'src/products/entities/product.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class OrderDetail {
  @PrimaryGeneratedColumn()
  order_detail_id: number;

  @Column({ default: '' })
  ord_detail_name: string;

  @Column({ default: 0 })
  ord_detail_amount: number;

  @Column({ type: 'float', default: 0.0 })
  ord_detail_total: number;

  @Column({ type: 'float', default: 0.0 })
  ord_detail_price: number;

  @Column({ default: 'กำลังรอคิว' })
  ord_detail_status: string;

  @Column({ default: 0 })
  ord_detail_table: number;

  @CreateDateColumn()
  createDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Product, (product) => product.orderDetails)
  products: Product;

  @ManyToOne(() => OrderItem, (orderItem) => orderItem.orderDetails)
  orderItems: OrderItem;
}
