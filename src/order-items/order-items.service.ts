import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOrderItemDto } from './dto/create-order-item.dto';
import { UpdateOrderItemDto } from './dto/update-order-item.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderItem } from './entities/order-item.entity';
import { Repository } from 'typeorm';
import { OrderDetail } from 'src/order-details/entities/order-detail.entity';
import { Product } from 'src/products/entities/product.entity';
import { Order } from 'src/orders/entities/order.entity';

@Injectable()
export class OrderItemsService {
  constructor(
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
    @InjectRepository(OrderDetail)
    private orderDetailRepository: Repository<OrderDetail>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
  ) {}

  async create(createOrderItemDto: CreateOrderItemDto) {
    const orderItem: OrderItem = new OrderItem();

    orderItem.orders = await this.orderRepository.findOneBy({
      order_id: createOrderItemDto.orderId,
    });
    orderItem.ord_order = orderItem.orders.order_id;
    orderItem.ord_item_table = orderItem.orders.ord_table;
    await this.orderItemRepository.save(orderItem);

    for (const odi of createOrderItemDto.orderDetail) {
      const orderDetail = new OrderDetail();
      orderDetail.ord_detail_table = orderItem.ord_item_table;
      await this.orderDetailRepository.save(orderDetail);
      orderDetail.ord_detail_amount = odi.ord_detail_amount;
      orderDetail.products = await this.productRepository.findOneBy({
        product_id: odi.productId,
      });
      orderDetail.ord_detail_name = orderDetail.products.name;
      orderDetail.ord_detail_price = orderDetail.products.price;
      orderDetail.ord_detail_total =
        orderDetail.ord_detail_price * orderDetail.ord_detail_amount;
      orderDetail.orderItems = orderItem;

      await this.orderDetailRepository.save(orderDetail);
      orderItem.ord_item_amount =
        orderItem.ord_item_amount + orderDetail.ord_detail_amount;
      orderItem.ord_item_total =
        orderItem.ord_item_total + orderDetail.ord_detail_total;

      await this.orderDetailRepository.save(orderDetail);
    }

    await this.orderItemRepository.save(orderItem);
    return await this.orderItemRepository.findOne({
      where: { order_item_id: orderItem.order_item_id },
      relations: ['orderDetails', 'orders'],
    });
  }

  findAll() {
    return this.orderItemRepository.find();
  }

  findOne(id: number) {
    return this.orderItemRepository.findOne({
      where: { order_item_id: id },
      relations: ['orders', 'orderDetails'],
    });
  }

  async getOrderItembyTableId(id: number) {
    const orderItem = await this.orderItemRepository.find({
      where: { ord_item_table: id },
      relations: ['orderDetails'],
    });
    if (!orderItem) {
      throw new NotFoundException(`No OrderItem in Order ${id}`);
    }
    return orderItem;
  }

  async update(id: number, updateOrderItemDto: UpdateOrderItemDto) {
    const orderItem = await this.orderItemRepository.findOneBy({
      order_item_id: id,
    });
    const updateOrderItem = { ...orderItem, ...updateOrderItemDto };
    return this.orderItemRepository.save(updateOrderItem);
  }

  async remove(id: number) {
    const orderItem = await this.orderItemRepository.findOneBy({
      order_item_id: id,
    });
    return this.orderItemRepository.softRemove(orderItem);
  }
}
