import { OrderDetail } from 'src/order-details/entities/order-detail.entity';
import { Order } from 'src/orders/entities/order.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class OrderItem {
  [x: string]: any;
  @PrimaryGeneratedColumn()
  order_item_id: number;

  @Column({ default: 0 })
  ord_item_amount: number;

  @Column({ type: 'float', default: 0.0 })
  ord_item_total: number;

  @Column({ default: 0 })
  ord_item_table: number;

  @Column({ default: 0 })
  ord_order: number;

  @CreateDateColumn()
  createDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Order, (order) => order.orderItems)
  orders: Order;

  @OneToMany(() => OrderDetail, (orderDetail) => orderDetail.orderItems)
  orderDetails: OrderDetail;
}
