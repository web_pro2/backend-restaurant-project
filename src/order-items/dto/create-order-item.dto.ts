import { CreateOrderDetailDto } from 'src/order-details/dto/create-order-detail.dto';
import { Order } from 'src/orders/entities/order.entity';

export class CreateOrderItemDto {
  ord_item_amount: 0;

  ord_item_total: 0;

  ord_item_table: number;

  orderDetailId: number;

  order: Order;

  orderId: number;

  orderDetail: CreateOrderDetailDto[];
}
