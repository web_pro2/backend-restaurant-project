import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from 'src/users/users.module';
import { LocalStrategy } from './local.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    // .register({
    //   defaultStrategy: 'jwt', // กำหนดค่า default strategy สำหรับ PassportModule
    //   property: 'user', // กำหนดชื่อ property สำหรับเก็บข้อมูล user ใน req object
    //   session: true, // กำหนดให้ PassportModule เก็บ session ของผู้ใช้งาน
    // })
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '4w' },
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
