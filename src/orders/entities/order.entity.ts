import { OrderItem } from 'src/order-items/entities/order-item.entity';
import { Table } from 'src/tables/entities/table.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  order_id: number;

  @Column({ default: 0 })
  ord_amount: number;

  @Column({ type: 'float', default: 0.0 })
  ord_total: number;

  @Column({ type: 'float', default: 0.0 })
  ord_cash: number;

  @Column({ type: 'float', default: 0.0 })
  ord_change: number;

  @Column({ default: 0 })
  ord_table: number;

  @CreateDateColumn()
  createDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Table, (table) => table.orders)
  tables: Table;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.orders)
  orderItems: OrderItem[];
}
