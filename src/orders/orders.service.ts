import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import { Table } from 'src/tables/entities/table.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';
import { OrderDetail } from 'src/order-details/entities/order-detail.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Table)
    private tableRepository: Repository<Table>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
    @InjectRepository(OrderDetail)
    private orderDetailRepository: Repository<OrderDetail>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const order: Order = new Order();
    order.tables = await this.tableRepository.findOneBy({
      id: createOrderDto.tableId,
    });
    order.ord_table = order.tables.id;
    await this.orderRepository.save(order);
    if (createOrderDto.tableId != undefined) {
      const table_1 = await this.tableRepository.findOneBy({
        id: order.tables.id,
      });
      if (table_1.status == 'ready') {
        table_1.status = 'used';
        await this.tableRepository.update(table_1.id, table_1);
      }
    }
    return order;
  }
  // const table = await this.tableRepository.findOne({
  //   where: {
  //     id: createOrderDto.table.id,
  //   },
  // });

  // // const table = await this.tableRepository.findOneBy({
  // //   id: createOrderDto.tableId.id,
  // // });
  // const order: Order = new Order();
  // order.tables = table;
  // await this.orderRepository.save(order);

  findAll() {
    return this.orderRepository.find({ relations: ['orderItems', 'tables'] });
  }

  findOne(id: number) {
    return this.orderRepository.findOne({
      where: { order_id: id },
      relations: ['orderItems', 'tables'],
    });
  }

  async getOrderbyTableId(id: number) {
    const order = await this.orderRepository.findOne({
      where: { ord_table: id },
      relations: ['orderItems', 'tables'],
    });
    if (!order) {
      throw new NotFoundException(`No Order in Table ${id}`);
    }
    return order;
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    const order = await this.orderRepository.findOne({
      where: { order_id: id },
      relations: ['tables'],
    });
    if (updateOrderDto.tableId != undefined) {
      const table_0 = await this.tableRepository.findOneBy({
        id: order.tables.id,
      });
      const table_1 = await this.tableRepository.findOneBy({
        id: updateOrderDto.tableId,
      });
      if (table_0.status == 'used' && table_1.status == 'ready') {
        table_0.status = 'clean';
        await this.tableRepository.update(table_0.id, table_0);
        table_1.status = 'used';
        await this.tableRepository.update(table_1.id, table_1);
        order.tables = await this.tableRepository.findOneBy({
          id: updateOrderDto.tableId,
        });
        order.ord_table = order.tables.id;
        // if (!!updateOrderDto.orderItems) {
        //   const orderItems: OrderItem[] = updateOrderDto.orderItems.map(
        //     (od) => {
        //       const orderItem = new OrderItem();
        //       orderItem.ord_item_amount = od.ord_item_amount;
        //       orderItem.ord_item_table = od.ord_item_table;
        //       orderItem.ord_item_total = od.ord_item_total;
        //       return orderItem;
        //     },
        //   );
        //   order.orderItems = orderItems;
        //   await this.orderRepository.save(order);

        //   // สร้าง OrderDetail จาก OrderItem แต่ละรายการ
        //   const orderDetails: OrderDetail[] = orderItems.map((oi) => {
        //     const orderDetail = new OrderDetail();
        //     orderDetail.ord_detail_table = oi.ord_item_table;
        //     // กำหนดค่าอื่น ๆ ตามต้องการ
        //     return orderDetail;
        //   });
        //   await this.orderDetailRepository.save(orderDetails);
        // }
        //
        // if (!!updateOrderDto.orderItems) {
        // const orderItems: OrderItem[] = updateOrderDto.orderItems.map((od) => {
        //   const orderItem = new OrderItem();
        //   orderItem.ord_item_amount = od.ord_item_amount;
        //   orderItem.ord_item_table = od.ord_item_table;
        //   orderItem.ord_item_total = od.ord_item_total;
        //   // orderItem.orderDetails = [];
        //   return orderItem;
        // });

        // // save orderItems
        // await this.orderItemRepository.save(orderItems);

        // // create orderDetails array with one item for each orderItem
        // const orderDetails: OrderDetail[] = orderItems.map((oi) => {
        //   const orderDetail = new OrderDetail();
        //   orderDetail.ord_detail_table = oi.ord_item_table;
        //   // set other properties as needed
        //   return orderDetail;
        // });

        // assign orderDetails to corresponding orderItems
        // orderItems.forEach((oi, index) => {
        //   oi.orderDetails.push(orderDetails[index]);
        // });

        // save orderDetails
        // await this.orderDetailRepository.save(orderDetails);
      }

      await this.orderRepository.save(order);
    }

    if (updateOrderDto.orderItems) {
      const orderItems: OrderItem[] = updateOrderDto.orderItems.map((od) => {
        const orderItem = new OrderItem();
        orderItem.ord_item_amount = od.ord_item_amount;
        orderItem.ord_item_table = od.ord_item_table;
        orderItem.ord_item_total = od.ord_item_total;
        return orderItem;
      });

      orderItems.forEach((orderItem) => {
        order.ord_total += orderItem.ord_item_total;
        order.ord_amount += orderItem.ord_item_amount;
      });
      await this.orderRepository.save(order);
    }

    await this.orderRepository.save(order);
    return this.orderRepository.findOne({
      where: { order_id: order.order_id },
      relations: ['orderItems', 'tables', 'orderItems.orderDetails'],
    });
  }

  async remove(id: number) {
    const order = await this.orderRepository.findOneBy({
      order_id: id,
    });
    return this.orderRepository.softRemove(order);
  }
}
