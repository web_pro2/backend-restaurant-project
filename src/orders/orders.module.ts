import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Table } from 'src/tables/entities/table.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';
import { OrderDetail } from 'src/order-details/entities/order-detail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, Table, OrderItem, OrderDetail])],
  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
