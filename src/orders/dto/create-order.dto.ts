import { CreateOrderItemDto } from 'src/order-items/dto/create-order-item.dto';
import { Table } from 'src/tables/entities/table.entity';

export class CreateOrderDto {
  ord_amount: 0;

  ord_total: 0;

  ord_cash: 0;

  ord_change: 0;

  tableId: number;

  table: Table;

  orderItems: CreateOrderItemDto[];
}
