import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
  ) {}

  create(createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
    const employee: Employee = new Employee();
    employee.name = createEmployeeDto.name;
    employee.surname = createEmployeeDto.surname;
    employee.tel = createEmployeeDto.tel;
    employee.jobtitle = createEmployeeDto.jobtitle;
    return this.employeesRepository.save(employee);
  }

  findAll(): Promise<Employee[]> {
    return this.employeesRepository.find();
  }

  findOne(id: number): Promise<Employee> {
    return this.employeesRepository.findOneBy({ employee_id: id });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const employee = await this.employeesRepository.findOneBy({
      employee_id: id,
    });
    const updateEmployee = { ...employee, ...updateEmployeeDto };
    return this.employeesRepository.save(updateEmployee);
  }

  async remove(id: number) {
    const employee = await this.employeesRepository.findOneBy({
      employee_id: id,
    });
    if (!employee) {
      throw new NotFoundException();
    }
    return this.employeesRepository.softRemove(employee);
  }
}
