import { IsNotEmpty } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  price: number;

  img = 'no_img_avaliable.jpg';

  @IsNotEmpty()
  categoryId: number;
}
