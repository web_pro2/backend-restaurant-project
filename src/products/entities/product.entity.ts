import { Category } from 'src/category/entities/category.entity';
import { OrderDetail } from 'src/order-details/entities/order-detail.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  product_id: number;

  @Column({ length: 64 })
  name: string;

  @Column({ type: 'float' })
  price: number;

  @Column({
    length: '128',
    default: 'no_img_avaliable.jpg',
  })
  img: string;

  @ManyToOne(() => Category, (category) => category.product)
  category: Category;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  @Column()
  categoryId: number;

  @OneToMany(() => OrderDetail, (orderDetail) => orderDetail.products)
  orderDetails: OrderDetail;
}
