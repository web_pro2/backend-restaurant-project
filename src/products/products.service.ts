import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/category/entities/category.entity';
import { Like, Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
  ) {}

  async create(createProductDto: CreateProductDto) {
    const category = await this.categoryRepository.findOne({
      where: {
        category_id: createProductDto.categoryId,
      },
    });
    const product: Product = new Product();
    product.categoryId = category.category_id;
    product.category = category;
    product.name = createProductDto.name;
    product.price = createProductDto.price;
    product.img = createProductDto.img;
    return this.productsRepository.save(product);
  }

  findByCategory(id: number, keyword: string) {
    const searchKeyword = keyword || '';
    return this.productsRepository.find({
      relations: ['category'],
      where: {
        categoryId: id,
        name: Like(`${searchKeyword}%`),
      },
    });
  }

  async findAll(query): Promise<Paginate> {
    const page = query.page || 1;
    const take = query.take || 100;
    const skip = (page - 1) * take;
    const keyword = query.keyword || '';
    const orderBy = query.orderBy || 'name';
    const order = query.order || 'ASC';
    const currentPage = page;
    const [result, total] = await this.productsRepository.findAndCount({
      relations: ['category'],
      where: { name: Like(`%${keyword}%`) },
      order: { [orderBy]: order },
      take: take,
      skip: skip,
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage,
    };
  }

  findAlls() {
    return this.productsRepository.findAndCount({
      relations: ['category'],
    });
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { product_id: id },
      relations: ['category'],
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRepository.findOneBy({ product_id: id });
    if (!product) {
      throw new NotFoundException();
    }
    const updatedProduct = { ...product, ...updateProductDto };
    return this.productsRepository.save(updatedProduct);
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOneBy({ product_id: id });
    if (!product) {
      throw new NotFoundException();
    }
    return this.productsRepository.softRemove(product);
  }
}
