import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { Product } from './entities/product.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from 'src/category/entities/category.entity';
import { OrderDetail } from 'src/order-details/entities/order-detail.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Product, Category, OrderDetail, OrderItem]),
  ],
  controllers: [ProductsController],
  providers: [ProductsService],
})
export class ProductsModule {}
