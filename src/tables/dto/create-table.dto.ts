import { IsNotEmpty, Length, IsPositive } from 'class-validator';
export class CreateTableDto {
  @IsNotEmpty()
  @IsPositive()
  chair: number;

  @IsNotEmpty()
  // @Length(8)
  status: string;
}
