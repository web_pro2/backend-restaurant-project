import { Order } from 'src/orders/entities/order.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Table {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({})
  chair: number;

  @Column({ length: 64 })
  status: string;

  @CreateDateColumn()
  table_createdAt: Date;

  @UpdateDateColumn()
  table_updatedAt: Date;

  @DeleteDateColumn()
  table_deletedAt: Date;

  @OneToMany(() => Order, (order) => order.tables)
  orders: Order;
}
