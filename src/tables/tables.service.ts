import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';

@Injectable()
export class TablesService {
  constructor(
    @InjectRepository(Table)
    private tableRepository: Repository<Table>,
  ) {}

  create(createTableDto: CreateTableDto): Promise<Table> {
    const table: Table = new Table();
    table.chair = createTableDto.chair;
    table.status = createTableDto.status;
    return this.tableRepository.save(table);
  }

  async findAll(query): Promise<Paginate> {
    const page = query.page || 1;
    const take = query.take || 100;
    const skip = (page - 1) * take;
    const currentPage = page;
    const [result, total] = await this.tableRepository.findAndCount({
      relations: ['orders'],
      take: take,
      skip: skip,
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage,
    };
  }

  findAlls() {
    return this.tableRepository.find();
  }

  findOne(id: number): Promise<Table> {
    return this.tableRepository.findOneBy({ id: id });
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    const table = await this.tableRepository.findOneBy({ id: id });
    const updateTable = { ...table, ...updateTableDto };
    return this.tableRepository.save(updateTable);
  }

  async remove(id: number) {
    const table = await this.tableRepository.findOneBy({ id: id });
    if (!table) {
      throw new NotFoundException();
    }
    return this.tableRepository.softRemove(table);
  }
}
